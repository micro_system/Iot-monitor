package cn.ljobin.bibi.mapper;

import cn.ljobin.bibi.domain.monitor.MonitorEmail;
import cn.ljobin.bibi.domain.monitor.MonitorMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 盆栽信息Mapper接口
 * 
 * @author lyb
 * @date 2020-04-21
 */
@Mapper
public interface MonitorMessageMapper extends BaseMapper<MonitorMessage>
{
}
