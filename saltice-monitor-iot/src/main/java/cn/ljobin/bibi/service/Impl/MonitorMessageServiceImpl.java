package cn.ljobin.bibi.service.Impl;

import cn.ljobin.bibi.domain.monitor.MonitorEmail;
import cn.ljobin.bibi.domain.monitor.MonitorMessage;
import cn.ljobin.bibi.mapper.EmailMapper;
import cn.ljobin.bibi.mapper.MonitorMessageMapper;
import cn.ljobin.bibi.service.IEmailService;
import cn.ljobin.bibi.service.IMonitorMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 盆栽信息Service业务层处理
 * 
 * @author lyb
 * @date 2020-04-21
 */
@Service
public class MonitorMessageServiceImpl extends ServiceImpl<MonitorMessageMapper, MonitorMessage> implements IMonitorMessageService
{
}
