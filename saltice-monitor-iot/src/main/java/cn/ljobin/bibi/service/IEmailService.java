package cn.ljobin.bibi.service;

import cn.ljobin.bibi.domain.monitor.MonitorEmail;
import cn.ljobin.bibi.domain.monitor.MonitorMessage;
import cn.ljobin.bibi.domain.pot.TbIoterminal;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * email发送服务
 * 
 * @author lyb
 * @date 2020-04-21
 */
public interface IEmailService extends IService<MonitorEmail> {
    /**
     * 消费消息
     * @param monitorMessage
     */
    public void recodeMonitorMsg(MonitorMessage monitorMessage);
}
