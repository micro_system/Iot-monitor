package cn.ljobin.bibi.service;

import cn.ljobin.bibi.domain.monitor.MonitorEmail;
import cn.ljobin.bibi.domain.monitor.MonitorEmailRepush;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 盆栽信息Service接口
 * 
 * @author lyb
 * @date 2020-04-21
 */
public interface IEmailRePushService extends IService<MonitorEmailRepush>
{
}
