package cn.ljobin.bibi.service;

import cn.ljobin.bibi.domain.monitor.MonitorEmail;
import cn.ljobin.bibi.domain.monitor.MonitorMessage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 盆栽信息Service接口
 * 
 * @author lyb
 * @date 2020-04-21
 */
public interface IMonitorMessageService extends IService<MonitorMessage>
{
}
