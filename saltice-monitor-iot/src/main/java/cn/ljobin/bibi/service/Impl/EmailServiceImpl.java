package cn.ljobin.bibi.service.Impl;

import cn.ljobin.bibi.domain.monitor.MonitorEmail;
import cn.ljobin.bibi.domain.monitor.MonitorMessage;
import cn.ljobin.bibi.mapper.EmailMapper;
import cn.ljobin.bibi.service.IEmailService;
import cn.ljobin.bibi.utils.email.BaseMailSend;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/**
 * 盆栽信息Service业务层处理
 * 
 * @author lyb
 * @date 2020-04-21
 */
@Service
public class EmailServiceImpl extends ServiceImpl<EmailMapper, MonitorEmail> implements IEmailService {
    /**
     * 发送邮件
     */
    //@Resource(name = "sendQqMailUtil")
    @Resource(name = "sprBooEmailUtil")
    private BaseMailSend sendQQMailUtil;

    @Override
    public void recodeMonitorMsg(MonitorMessage monitorMessage) {
        sendQQMailUtil.sendEmail(monitorMessage);
    }
}
