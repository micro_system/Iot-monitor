package cn.ljobin.bibi.service.Impl;

import cn.ljobin.bibi.domain.monitor.MonitorEmailRepush;
import cn.ljobin.bibi.mapper.EmailRePushMapper;
import cn.ljobin.bibi.service.IEmailRePushService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 盆栽信息Service业务层处理
 * 
 * @author lyb
 * @date 2020-04-21
 */
@Service
public class EmailRePushServiceImpl extends ServiceImpl<EmailRePushMapper, MonitorEmailRepush> implements IEmailRePushService
{
}
