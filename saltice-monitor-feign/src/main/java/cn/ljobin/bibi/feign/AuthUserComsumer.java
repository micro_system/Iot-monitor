package cn.ljobin.bibi.feign;

import cn.ljobin.bibi.feign.fallback.AuthUserComsumeuFallback;
import cn.stylefeng.roses.kernel.model.api.AuthService;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @program: IoT-Plat
 * @description: 获取用户信息的
 * @author: Mr.Liu
 * @create: 2020-04-12 16:23
 **/
@FeignClient(value = "roses-system" , fallback = AuthUserComsumeuFallback.class)
public interface AuthUserComsumer extends AuthService {
}
