package cn.ljobin.bibi.domain.monitor;

import cn.ljobin.bibi.domain.monitor.enums.LinkInfoType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * @program: Iot-Monitor
 * @description: 发送失败的进行重试
 * @author: Mr.Liu
 * @create: 2020-06-02 11:46
 **/
@Data
@ToString
@TableName("emails_repush")
public class MonitorEmailRepush {
    /**
     * id
     * */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    /**
     * 联系信息
     * **/
    @TableField("linkInfo")
    private String linkInfo;
    /**
     * 联系信息类型 枚举
     * **/
    @TableField(exist = false)
    private LinkInfoType linkInfoType;
    /**
     * 联系信息类型
     */
    @TableField("type")
    private Integer type;
    /**
     * 消息体
     **/
    @TableField("body")
    private String body;
    /**
     * 上次发送时间
     * **/
    @TableField("oldPushTime")
    private Date oldPushTime;
    /**
     * 重试次数
     */
    @TableField("num")
    private Integer num;
    /**
     * 是否死亡 0-未 1，已死
     */
    @TableField("already_dead")
    private String alreadyDead;
}
