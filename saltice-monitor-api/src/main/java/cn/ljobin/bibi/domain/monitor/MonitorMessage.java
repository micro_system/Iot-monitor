package cn.ljobin.bibi.domain.monitor;

import cn.ljobin.bibi.domain.monitor.enums.LinkInfoType;
import cn.ljobin.bibi.enums.MessageType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;

import java.util.Date;

/**
 * @program: Iot-Monitor
 * @description:  报警消息实体
 * @author: Mr.Liu
 * @create: 2020-06-02 10:31
 **/
@Data
@ToString
@TableName("monitor")
public class MonitorMessage {
    /**消息id**/
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    /**
     * 客户端id
     */
    @TableField("clientId")
    private String clientId;
    /**
     * 联系信息
     */
    @TableField("linkInfo")
    private String linkInfo;
    /**
     * 联系信息类型 枚举
     * **/
    @TableField(exist = false)
    private LinkInfoType linkInfoType;
    /**
     * 消息类型
     **/
    @TableField("messageType")
    private Integer messageType;
    /**
     * 联系信息类型
     */
    @TableField("type")
    private Integer type;
    /**
     * 消息标题
     **/
    @TableField("title")
    private String title;
    /**
     * 消息内容
     * utf-8
     */
    @TableField("ploy")
    private String ploy;
    /**
     * 插入的时间
     * **/
    @TableField("insertTime")
    private Date insertTime;
}
