package cn.ljobin.bibi.domain.wapper;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * @program: IoT-Plat
 * @description: 包装类
 * @author: Mr.Liu
 * @create: 2020-04-22 11:10
 **/
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class TbIoterminalWapper {
    private static final long serialVersionUID = 1L;


    /** 盆栽终端IMEI号 */
    private String imei;

    /** IP地址 */
    private String conip;

    /** 当前数据接收状态,0-关 1-开 */
    private String status;

    /** 产出时间 */
    private Date protime;

    /** 启用时间 */
    private Date starttime;

    /** 存放位置 */
    private String address;

    /** 备注 */
    private String demo;

    /** 最后通信时间 */
    private Date enddate;

    /** 0-离线，1-在线 */
    private String online;
    /**
     * 盆栽自定义名称
     */
    private String potName;
    /**
     * 用户名称
     * 不是数据库中的字段
     */
    private String uname;
    /**
     * 类型名称
     * 不是数据库中的字段
     */
    private String typeName;
    /**
     * 盆栽类型id
     */
    private Integer pType;
    /**
     * 是否是享受来的盆栽
     */
    private Boolean enjoyPot;

    public TbIoterminalWapper(String imei, String conip, String status, Date protime, Date starttime, String address, String demo, Date enddate, String online, String potName, String uname, String typeName, Integer pType) {
        this.imei = imei;
        this.conip = conip;
        this.status = status;
        this.protime = protime;
        this.starttime = starttime;
        this.address = address;
        this.demo = demo;
        this.enddate = enddate;
        this.online = online;
        this.potName = potName;
        this.uname = uname;
        this.typeName = typeName;
        this.pType = pType;
    }
}
