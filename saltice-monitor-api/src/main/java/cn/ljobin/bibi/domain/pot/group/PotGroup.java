package cn.ljobin.bibi.domain.pot.group;

import javax.validation.groups.Default;

/**
 * @program: IoT-Plat
 * @description: 用户分组
 * @author: Mr.Liu
 * @create: 2020-04-29 16:37
 **/

public interface PotGroup extends Default {
}
