package cn.ljobin.bibi.domain.pot;

import cn.ljobin.bibi.domain.pot.group.PotGroup;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 * 盆栽信息对象 tb_ioterminal
 * 
 * @author lyb
 * @date 2020-04-21
 */
@TableName("tb_ioterminal")
@ApiModel(value = "盆栽对象", description = "pot")
public class TbIoterminal
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /** 盆栽终端IMEI号 */
    @TableField("imei")
    //@Length(min = 10,max = 20,message = "盆栽IMEI长度不合格",groups = {PotGroup.class})
    @Pattern(regexp = "^[A-Za-z0-9]{10,20}$", message = "盆栽IMEI限制：最多20字符，包含字母和数字",groups = {PotGroup.class})
    @ApiModelProperty(value = "盆栽终端IMEI号", required = true)
    private String imei;

    /** 类型 */
    private Integer ctype;

    /** IP地址 */
    private String conip;

    /** 当前数据接收状态,0-关 1-开 */
    private String status;

    /** 产出时间 */
    private Date protime;

    /** 启用时间 */
    private Date starttime;

    /** 存放位置编号 */
    private Long deptid;

    /** 使用人编号 */
    private Long uid;

    /** 备注 */
    @ApiModelProperty(value = "盆栽介绍", required = true)
    private String demo;

    /** 0-无效,1-有效 */
    private String delflag;

    /** 最后通信时间 */
    private Date enddate;

    /** 0-离线，1-在线 */
    private String online;
    /**
     * 盆栽自定义名称
     */
    @ApiModelProperty(value = "盆栽名称", required = true)
    @TableField("pot_name")
    private String potName;
    /**
     * 用户名称
     * 不是数据库中的字段
     */
    @TableField(exist = false)
    private String uname;
    /**
     * 类型名称
     * 不是数据库中的字段
     */
    @TableField(exist = false)
    private String typeName;

    /**
     * 是否是享受来的盆栽
     */
    @TableField(exist = false)
    private Boolean enjoyPot;

    public Boolean getEnjoyPot() {
        return enjoyPot;
    }

    public void setEnjoyPot(Boolean enjoyPot) {
        this.enjoyPot = enjoyPot;
    }

    public String getPotName() {
        return potName;
    }

    public void setPotName(String potName) {
        this.potName = potName;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setImei(String imei) 
    {
        this.imei = imei;
    }

    public String getImei() 
    {
        return imei;
    }
    public void setCtype(Integer ctype)
    {
        this.ctype = ctype;
    }

    public Integer getCtype()
    {
        return ctype;
    }
    public void setConip(String conip) 
    {
        this.conip = conip;
    }

    public String getConip() 
    {
        return conip;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setProtime(Date protime) 
    {
        this.protime = protime;
    }

    public Date getProtime() 
    {
        return protime;
    }
    public void setStarttime(Date starttime) 
    {
        this.starttime = starttime;
    }

    public Date getStarttime() 
    {
        return starttime;
    }
    public void setDeptid(Long deptid) 
    {
        this.deptid = deptid;
    }

    public Long getDeptid() 
    {
        return deptid;
    }
    public void setUid(Long uid) 
    {
        this.uid = uid;
    }

    public Long getUid() 
    {
        return uid;
    }
    public void setDemo(String demo) 
    {
        this.demo = demo;
    }

    public String getDemo() 
    {
        return demo;
    }
    public void setDelflag(String delflag) 
    {
        this.delflag = delflag;
    }

    public String getDelflag() 
    {
        return delflag;
    }
    public void setEnddate(Date enddate) 
    {
        this.enddate = enddate;
    }

    public Date getEnddate() 
    {
        return enddate;
    }
    public void setOnline(String online) 
    {
        this.online = online;
    }

    public String getOnline() 
    {
        return online;
    }

    @Override
    public String toString() {
        return "TbIoterminal{" +
                "id=" + id +
                ", imei='" + imei + '\'' +
                ", ctype=" + ctype +
                ", conip='" + conip + '\'' +
                ", status='" + status + '\'' +
                ", protime=" + protime +
                ", starttime=" + starttime +
                ", deptid=" + deptid +
                ", uid=" + uid +
                ", demo='" + demo + '\'' +
                ", delflag='" + delflag + '\'' +
                ", enddate=" + enddate +
                ", online='" + online + '\'' +
                ", uname='" + uname + '\'' +
                ", typeName='" + typeName + '\'' +
                '}';
    }
}
