package cn.ljobin.bibi.domain.monitor.enums;

/**
 * @program: Iot-Monitor
 * @description: 联系信息类型
 * @author: Mr.Liu
 * @create: 2020-06-02 10:36
 **/
public enum LinkInfoType {
    PHONE(1,"phone"),
    EMAIL(2,"email");
    /**
     * 类型id
     */
    private int id;
    /**
     * 类型名称
     */
    private String type;
    private LinkInfoType(int id,String type){
        this.id=id;
        this.type=type;
    }

    public int getId() {
        return id;
    }


    public String getType() {
        return type;
    }
}
