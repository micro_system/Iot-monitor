package cn.ljobin.bibi.api;

import cn.ljobin.bibi.domain.monitor.MonitorMessage;
import org.springframework.web.bind.annotation.*;

/**
 * @program: IoT-Plat
 * @description:
 * @author: Mr.Liu
 * @create: 2020-04-11 15:26
 **/
@RequestMapping("/monitor")
public interface MonitorApi {
    /**
     * 报警
     * @param monitorMessage 报警消息
     * @return 成功与否
     */
    @GetMapping("/callPolice")
    public boolean callPolice(MonitorMessage monitorMessage);
}
