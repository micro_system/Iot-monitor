package cn.ljobin.bibi.enums;

/**
 * 消息队列的枚举
 *
 * @author lyb
 * @Date 2020年5月6日 12:30:48
 */
public enum MessageQueueEnum {

    /**
     * 下单的消息队列，用于保存需要下发的邮箱
     */
    MAKE_MONITOR_MESSAGE
}
