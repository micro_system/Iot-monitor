package cn.ljobin.bibi.enums;

/**
 * @program: Iot-Monitor
 * @description: 消息类型
 * @author: Mr.Liu
 * @create: 2020-06-06 16:02
 **/
public class MessageType {
    public static final int SUCCESS = 0;
    public static final int ERROR = 1;
    public static final int WANNER = 2;
}
