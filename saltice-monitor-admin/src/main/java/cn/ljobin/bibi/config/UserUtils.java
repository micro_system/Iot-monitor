package cn.ljobin.bibi.config;

import cn.stylefeng.roses.core.util.HttpContext;
import cn.stylefeng.roses.system.utils.AccountUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @program: IoT-Plat
 * @description: 用户相关操作
 * @author: Mr.Liu
 * @create: 2020-05-07 16:43
 **/
@Component
public class UserUtils {
    @Autowired
    private AccountUtils accountUtils;

    /**
     * 获得用户id，从token里面
     * @param request
     * @return
     */
    public Long getUid(HttpServletRequest request) {
        String token = HttpContext.getTokenFromRequest(request);
        //方法三
        return accountUtils.getAccount(token);
    }
}
