package cn.ljobin.bibi.config;
import cn.ljobin.bibi.enums.MessageQueueEnum;
import cn.ljobin.bibi.listener.MonitorMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import javax.jms.ConnectionFactory;

/**
 * 消息队列监听配置
 *
 * @author fengshuonan
 * @date 2018-05-06 13:11
 */
@Configuration
public class QueueListenerConfig {

    @Autowired
    private MonitorMessageListener monitorMessageListener;

    @Bean
    public DefaultMessageListenerContainer messageListenerContainer(ConnectionFactory connectionFactory) {
        DefaultMessageListenerContainer factory = new DefaultMessageListenerContainer();
        factory.setConnectionFactory(connectionFactory);
        factory.setDestinationName(MessageQueueEnum.MAKE_MONITOR_MESSAGE.name());
        factory.setMessageListener(monitorMessageListener);
        return factory;
    }
}
