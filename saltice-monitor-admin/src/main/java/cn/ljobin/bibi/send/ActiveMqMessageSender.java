package cn.ljobin.bibi.send;

import cn.ljobin.bibi.domain.monitor.MonitorMessage;
import cn.ljobin.bibi.enums.MessageQueueEnum;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jms.JmsProperties;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

/**
 * activemq实现的消息发送器
 *
 * @author fengshuonan
 * @date 2018-04-22 22:03
 */
@Component
public class ActiveMqMessageSender {

    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendMessage(MonitorMessage reliableMessage) {

        jmsTemplate.setDefaultDestinationName(MessageQueueEnum.MAKE_MONITOR_MESSAGE.name());

        //设置ack确认为client方式
        jmsTemplate.setSessionAcknowledgeMode(JmsProperties.AcknowledgeMode.CLIENT.getMode());

        //发送消息
        jmsTemplate.send(new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                return session.createTextMessage(JSON.toJSONString(reliableMessage));
            }
        });
    }
}
