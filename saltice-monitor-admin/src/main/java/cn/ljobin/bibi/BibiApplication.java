package cn.ljobin.bibi;

import cn.ljobin.bibi.domain.monitor.MonitorMessage;
import cn.ljobin.bibi.domain.monitor.enums.LinkInfoType;
import cn.ljobin.bibi.enums.MessageType;
import cn.ljobin.bibi.send.ActiveMqMessageSender;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.spring4all.swagger.EnableSwagger2Doc;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * @program: nettyTCP
 * @description:
 * @author: Mr.Liu
 * @create: 2019-08-02 17:56
 **/
/**如果有其它的包需要被扫描，就在下面添加**/
/**cn.stylefeng.roses.kernel.logger.api是日志异步操作的**/
@SpringBootApplication(scanBasePackages = {"cn.ljobin.bibi","cn.stylefeng.roses"},exclude = {DruidDataSourceAutoConfigure.class})
/**开启定时器任务**/
@EnableScheduling
@EnableFeignClients
@EnableDiscoveryClient
@MapperScan(basePackages = "cn.ljobin.bibi.mapper")
@EnableSwagger2Doc
public class BibiApplication {
    @Autowired
    private ActiveMqMessageSender sender;
    //@Scheduled(fixedDelay = 3000)
    public void schedule(){
        MonitorMessage monitorMessage = new MonitorMessage();
        monitorMessage.setClientId("123778");
        monitorMessage.setMessageType(MessageType.SUCCESS);
        monitorMessage.setTitle("有小偷，快跑，测试。。。");
        monitorMessage.setLinkInfo("1337792659@qq.com");
        monitorMessage.setLinkInfoType(LinkInfoType.EMAIL);
        monitorMessage.setType(LinkInfoType.EMAIL.getId());
        monitorMessage.setPloy("啦啦啦啦啦啦啦啦啦啦零零零零了");
        sender.sendMessage(monitorMessage);
    }
    private final static Logger logger = LoggerFactory.getLogger(BibiApplication.class);
    public static ConfigurableApplicationContext applicationContext;
    public static void main(String[] args) {
        applicationContext =  SpringApplication.run(BibiApplication.class, args);
        logger.info("系统启动成功");
    }

}
