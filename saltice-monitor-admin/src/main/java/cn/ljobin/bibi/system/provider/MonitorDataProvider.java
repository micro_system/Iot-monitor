package cn.ljobin.bibi.system.provider;

import cn.ljobin.bibi.api.MonitorApi;
import cn.ljobin.bibi.domain.monitor.MonitorMessage;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: IoT-SC
 * @description:
 * @author: Mr.Liu
 * @create: 2020-04-11 15:28
 **/
@RestController
@Slf4j
@Api("报警api")
public class MonitorDataProvider implements MonitorApi {

    @Override
    public boolean callPolice(MonitorMessage monitorMessage) {
        return false;
    }
}
