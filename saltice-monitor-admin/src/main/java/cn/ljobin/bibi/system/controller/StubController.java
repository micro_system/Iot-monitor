package cn.ljobin.bibi.system.controller;

import cn.ljobin.bibi.domain.monitor.MonitorMessage;
import cn.ljobin.bibi.domain.monitor.enums.LinkInfoType;
import cn.ljobin.bibi.enums.MessageType;
import cn.ljobin.bibi.enums.execption.MessageExceptionEnum;
import cn.ljobin.bibi.service.IMonitorMessageService;
import cn.ljobin.bibi.stub.StubFactory;
import cn.ljobin.bibi.utils.email.BaseMailSend;
import cn.stylefeng.roses.core.reqres.response.ResponseData;
import cn.stylefeng.roses.kernel.model.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * @program: Iot-Monitor
 * @description:
 * @author: Mr.Liu
 * @create: 2020-06-06 23:50
 **/
@RestController
@Slf4j
@RequestMapping("/stub")
public class StubController {
    private static HashMap<String,BaseMailSend> obj = new HashMap<>();
    @Autowired
    private StubFactory stubFactory;
    @Autowired
    private IMonitorMessageService monitorMessageService;
    @GetMapping("/add")
    public String addStub(@RequestParam("className") String className){
        String imports = "import cn.ljobin.bibi.domain.monitor.MonitorMessage;\n" +
                "import cn.ljobin.bibi.enums.MessageType;\n" +
                "import cn.ljobin.bibi.utils.email.BaseMailSend;\n" +
                "import lombok.extern.slf4j.Slf4j;\n" +
                "import org.apache.commons.lang3.time.DateFormatUtils;\n" +
                "import org.slf4j.Logger;\n" +
                "import org.slf4j.LoggerFactory;\n" +
                "import org.springframework.core.io.ClassPathResource;\n" +
                "import org.springframework.mail.javamail.JavaMailSenderImpl;\n" +
                "import org.springframework.mail.javamail.MimeMessageHelper;\n" +
                "import org.springframework.stereotype.Service;\n" +
                "import javax.mail.MessagingException;\n" +
                "import javax.mail.internet.MimeMessage;\n" +
                "import java.io.BufferedReader;\n" +
                "import java.io.IOException;\n" +
                "import java.io.InputStream;\n" +
                "import java.io.InputStreamReader;\n" +
                "import java.text.MessageFormat;\n" +
                "import java.util.Date;\n" +
                "import java.util.Objects;\n" +
                "import java.util.Properties;\n" +
                "import java.io.IOException;\n" +
                "import java.util.concurrent.Executors;\n" +
                "import java.util.concurrent.LinkedBlockingQueue;\n" +
                "import java.util.concurrent.ScheduledExecutorService;\n" +
                "import java.util.concurrent.TimeUnit;\n";
        String statics="javaMailSender = new JavaMailSenderImpl();\n" +
                "        javaMailSender.setUsername(sendUserEmail);\n" +
                "        javaMailSender.setPassword(qqPassword);\n" +
                "        javaMailSender.setHost(\"smtp.qq.com\");\n" +
                "        javaMailSender.setPort(587);\n" +
                "        javaMailSender.setDefaultEncoding(\"UTF-8\");\n" +
                "        Properties props = new Properties();\n" +
                "        props.setProperty(\"mail.smtp.host\", \"smtp.qq.com\");\n" +
                "        props.setProperty(\"mail.transport.protocol\", \"smtp\");\n" +
                "        props.setProperty(\"mail.smtp.auth\", \"true\");\n" +
                "        props.setProperty(\"mail.smtp.connectiontimeout\", \"20000\");\n" +
                "        props.setProperty(\"mail.smtp.timeout\", \"20000\");\n" +
                "        javaMailSender.setJavaMailProperties(props);\n";
        String param="private static JavaMailSenderImpl javaMailSender;\n";
        String methodContent= "MimeMessage message = javaMailSender.createMimeMessage();\n" +
                "        MimeMessageHelper helper = null;\n" +
                "        try {\n" +
                "            helper = new MimeMessageHelper(message, true, \"UTF-8\");\n" +
                "            helper.setTo(new String[]{linkInfo});\n" +
                "            helper.setFrom(sendUserEmail);\n" +
                "            helper.setSubject(title);\n" +
                "            helper.setText(buildContent(title,content,type), true);\n" +
                "            String alarmIconName = \"success-alarm.png\";\n" +
                "            ClassPathResource img = new ClassPathResource(alarmIconName);\n" +
                "            helper.addInline(\"icon-alarm\", img);\n" +
                "            javaMailSender.send(message);\n" +
                "        } catch (MessagingException | IOException e) {\n" +
                "            e.printStackTrace();\n" +
                "        }\n" ;
        try {
            if(obj.get(className+"Stub")!=null){
                log.info("内存中已经有改实例");
                return "false";
            }
            BaseMailSend s = stubFactory.createStub(imports,className,param,statics,methodContent);
            obj.put(className+"Stub",s);
            log.info("添加实例成功");
            return "true";
        } catch (Exception e) {
            log.error("添加实例失败，{}，{}，{}，{}",imports,className,param,methodContent);
            e.printStackTrace();
            return "false";
        }
    }
    @PostMapping("/add2")
    public ResponseData addStub2(@RequestParam("className") String className,
                                 @RequestParam("imports") String imports,
                                 @RequestParam("statics") String statics,
                                 @RequestParam("param") String param,
                                 @RequestParam("methodContent") String methodContent){
        try {
            if(obj.get(className+"Stub")!=null){
                log.info("内存中已经有该实例");
                return ResponseData.success("内存中已经有该实例");
            }
            BaseMailSend s = stubFactory.createStub(imports,className,param,statics,methodContent);
            obj.put(className+"Stub",s);
            log.info("添加实例成功");
            return ResponseData.success("添加实例成功");
        } catch (Exception e) {
            log.error("添加实例失败，{}，{}，{}，{}",imports,className,param,methodContent);
            e.printStackTrace();
            return ResponseData.error("添加实例失败");
        }
    }
    @GetMapping("/send")
    public ResponseData send(@RequestParam("className") String className){
        if(obj.get(className+"Stub")==null){
            log.info("内存中没有该实例");
            return ResponseData.success("内存中没有该实例");
        }
        MonitorMessage monitorMessage = new MonitorMessage();
        monitorMessage.setClientId("123778");
        monitorMessage.setMessageType(MessageType.SUCCESS);
        monitorMessage.setTitle("有小偷，快跑，测试。。。");
        monitorMessage.setLinkInfo("1337792659@qq.com");
        monitorMessage.setLinkInfoType(LinkInfoType.EMAIL);
        monitorMessage.setType(LinkInfoType.EMAIL.getId());
        monitorMessage.setPloy("小屁孩");
        if(monitorMessageService.save(monitorMessage)){
            obj.get(className+"Stub").sendEmail(monitorMessage);
            return ResponseData.success("发送成功");
        }else {
            return ResponseData.error("发送失败");
        }
    }
}
