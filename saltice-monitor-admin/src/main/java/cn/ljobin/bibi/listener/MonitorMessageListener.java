package cn.ljobin.bibi.listener;

import cn.ljobin.bibi.domain.monitor.MonitorMessage;
import cn.ljobin.bibi.enums.execption.MessageExceptionEnum;
import cn.ljobin.bibi.service.IEmailService;
import cn.ljobin.bibi.service.IMonitorMessageService;
import cn.ljobin.bibi.service.Impl.MonitorMessageServiceImpl;
import cn.stylefeng.roses.core.util.ToolUtil;
import cn.stylefeng.roses.kernel.model.exception.ServiceException;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * @program: Iot-Monitor
 * @description: activeMq消费者
 * @author: Mr.Liu
 * @create: 2020-06-02 14:51
 **/
@Component
@Slf4j
public class MonitorMessageListener implements MessageListener {
    @Autowired
    private IMonitorMessageService monitorMessageService;
    @Autowired
    private IEmailService iEmailService;
    @Override
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            try {
                String messageBody = ((TextMessage) message).getText();
                if (ToolUtil.isEmpty(messageBody)) {
                    throw new ServiceException(MessageExceptionEnum.MESSAGE_BODY_CANT_EMPTY);
                }
                MonitorMessage flowParam = JSON.parseObject(messageBody, MonitorMessage.class);
                log.info(flowParam.toString());
                flowParam.setId(null);
                if(monitorMessageService.save(flowParam)){
                    iEmailService.recodeMonitorMsg(flowParam);
                }else {
                    throw new ServiceException(MessageExceptionEnum.MESSAGE_QUEUE_SAVE_ERROR);
                }
            } catch (JMSException ex) {
                throw new ServiceException(MessageExceptionEnum.MESSAGE_QUEUE_ERROR);
            }
        } else {
            throw new ServiceException(MessageExceptionEnum.MESSAGE_TYPE_ERROR);
        }
    }
}
