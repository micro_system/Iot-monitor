package cn.ljobin.bibi.constant;

/**
 * @program: IoT-Plat
 * @description: 状态常量
 * @author: Mr.Liu
 * @create: 2020-04-24 23:13
 **/
public class Status {
    public static String isEnable = "1";
    public static String noEnable = "0";
    public static String noDel = "0";
    public static String isDel = "1";
}
