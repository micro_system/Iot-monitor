package cn.ljobin.bibi.utils.email.impl;

import cn.ljobin.bibi.domain.monitor.MonitorMessage;
import cn.ljobin.bibi.enums.MessageType;
import cn.ljobin.bibi.utils.email.BaseMailSend;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Objects;
import java.util.Properties;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
/**
 * @program: Iot-Monitor
 * @description:
 * @author: Mr.Liu
 * @create: 2020-06-02 21:23
 **/
@Service("sprBooEmailUtil")
@Slf4j
public class SprBooEmailUtil implements BaseMailSend {
    private static String sendUserEmail = "2018214546@qq.com";
    private static String qqPassword = "ujpiarknosuybadb";
    private static JavaMailSenderImpl javaMailSender;
    private static String buffer;
    /**保存从activemq获取的消息*1024*1024 = 1048576*/
    public static final LinkedBlockingQueue<MonitorMessage> monitorMessages = new LinkedBlockingQueue<>(2<<20);
    /**上面那个满了就保存到下面这个**/
    public static final LinkedBlockingQueue<MonitorMessage> monitorMessages2 = new LinkedBlockingQueue<>(2<<20);
    public static final ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(10);
    static {
        //加载邮件html模板
        String fileName = "pod-scale-alarm.html";
        InputStream inputStream = ClassLoader.getSystemResourceAsStream(fileName);
        if(inputStream==null){
            log.error("{}文件读取失败",fileName);
        }else {
            BufferedReader fileReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder buffers = new StringBuilder();
            String line = "";
            try {
                while ((line = fileReader.readLine()) != null) {
                    buffers.append(line);
                }
                buffer = buffers.toString();
            } catch (Exception e) {
                log.error("读取文件失败，fileName:{}", fileName, e);
            } finally {
                try {
                    inputStream.close();
                    fileReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    static {
        javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setUsername(sendUserEmail);
        javaMailSender.setPassword(qqPassword);
        javaMailSender.setHost("smtp.qq.com");
        javaMailSender.setPort(587);
        javaMailSender.setDefaultEncoding("UTF-8");
        Properties props = new Properties();
        props.setProperty("mail.smtp.host", "smtp.qq.com");
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.connectiontimeout", "20000");
        props.setProperty("mail.smtp.timeout", "20000");
        javaMailSender.setJavaMailProperties(props);
    }
    static {
        for (int i = 0; i < 10; i++) {
            scheduledExecutorService.scheduleWithFixedDelay(()->{
                //弹出队顶元素，队列为空时返回null
                MonitorMessage monitorMessage = monitorMessages.poll();
                if(monitorMessage!=null){
                    send(monitorMessage.getTitle(),
                            monitorMessage.getPloy(),
                            monitorMessage.getLinkInfo(),
                            monitorMessage.getMessageType());
                }
            },0,100, TimeUnit.MILLISECONDS);
        }
        scheduledExecutorService.scheduleWithFixedDelay(()->{
            //弹出队顶元素，队列为空时返回null
            MonitorMessage monitorMessage = monitorMessages2.poll();
            if(monitorMessage!=null){
                send(monitorMessage.getTitle(),
                        monitorMessage.getPloy(),
                        monitorMessage.getLinkInfo(),
                        monitorMessage.getMessageType());
            }
        },0,10, TimeUnit.SECONDS);
    }
    @Override
    public void sendEmail(MonitorMessage monitorMessage) {
        //send(monitorMessage.getTitle(), monitorMessage.getPloy(), monitorMessage.getLinkInfo(),monitorMessage.getMessageType());
        try {
            monitorMessages.add(monitorMessage);
        }catch (IllegalStateException e){
            monitorMessages2.offer(monitorMessage);
        }
    }
    public static void send(String title, String content, String linkInfo, int type){
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message, true, "UTF-8");
            helper.setTo(new String[]{linkInfo});
//            helper.setCc("抄送人邮箱");
            helper.setFrom(sendUserEmail);
            helper.setSubject(title);
            helper.setText(buildContent(title,content,type), true);
            String alarmIconName = "success-alarm.png";
            ClassPathResource img = new ClassPathResource(alarmIconName);
            //设置图标
            helper.addInline("icon-alarm", img);
            /**邮件发送时间较久**/
            long st1 = System.currentTimeMillis();
            javaMailSender.send(message);
            long st2 = System.currentTimeMillis();
            log.info("发送耗时：{}",(st2-st1));
        } catch (MessagingException | IOException e) {
            e.printStackTrace();
        }
    }

        private static String buildContent(String title,String body,int isDanger) throws IOException {

            String contentText = body+"</br>以下是物联网设备详细信息, 敬请查看.<br>below is the information of service instance scale, please check. ";
            //邮件表格header
            String header = "<td>分区(Namespace)</td><td>服务(Service)</td><td>伸缩结果(Scale Result)</td><td>伸缩原因(Scale Reason)</td><td>当前实例数(Pod instance number)</td>";
            //绿色
            String emailHeadColor;
            switch (isDanger){
                case MessageType.ERROR:
                    emailHeadColor = "#cc1d0c";
                    break;
                case MessageType.SUCCESS:
                    emailHeadColor = "#10fa81";
                    break;
                case MessageType.WANNER:
                    emailHeadColor = "yellow";
                    break;
                default:
                    emailHeadColor = "#aaa";
            }
            String date = DateFormatUtils.format(new Date(), "yyyy/MM/dd HH:mm:ss");
            //填充html模板中的五个参数
            String linesBuffer = "<tr><td>" + "湖南分区" + "</td><td>" + "集群服务" + "</td><td>" + "服务暂停" + "</td>" +
                    "<td>" + "异常" + "</td><td>" + "2" + "</td></tr>";
            String htmlText = MessageFormat.format(buffer, title,emailHeadColor, contentText, date, header, linesBuffer);

            //改变表格样式
            htmlText = htmlText.replaceAll("<td>", "<td style=\"padding:6px 10px; line-height: 150%;\">");
            htmlText = htmlText.replaceAll("<tr>", "<tr style=\"border-bottom: 1px solid #eee; color:#666;\">");
            return htmlText;
        }

}
