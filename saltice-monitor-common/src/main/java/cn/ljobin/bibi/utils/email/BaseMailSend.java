package cn.ljobin.bibi.utils.email;

import cn.ljobin.bibi.domain.monitor.MonitorMessage;

/**
 * @program: Iot-Monitor
 * @description:
 * @author: Mr.Liu
 * @create: 2020-06-02 15:34
 **/

public interface BaseMailSend {
    /**
     * 发送邮件
     * @param monitorMessage 邮件标题
     */
    public void sendEmail(MonitorMessage monitorMessage);
}
