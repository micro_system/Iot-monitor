package cn.ljobin.bibi.utils;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Objects;

/**
 * @program: IoT-Plat
 * @description: 包装验证类
 * @author: Mr.Liu
 * @create: 2020-04-11 16:25
 **/
public class  AnnotationUtils<T> {
    /**
     * 反射包装QueryWrapper,自动将简单的条件查询 映射好
     * 构建where后面的条件
     */
    public  QueryWrapper<T>  wapperData (QueryWrapper<T> queryWrapper,T o){
        Class clazz = o.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field f : fields) {
            TableField tableField = f.getAnnotation(TableField.class);
            //没有注解表示肯定存在这个字段在数据库
            if(tableField==null){
                TableId tableId = f.getAnnotation(TableId.class);
                String name = "";
                if(tableId != null){
                    String n  = tableId.value();
                    if("".equals(n)){
                        name = f.getName();
                    }else {
                        name = n;
                    }
                }else {
                    name = f.getName();
                }
                Object value = getValue(o,f);
                if(value!=null && value != ""){
                    //System.out.println(name+"---"+value);
                    queryWrapper.eq(name,value);
                }
                continue;
            }
            //如果注解明确说明是不存在的，就不管了
            if(!tableField.exist()){
                continue;
            }
            //获取注解中的value值，及数据库中字段名称
            String name = tableField.value();
            if(!"".equals(name)){
                Object value = getValue(o,f);
                if(Objects.nonNull(value)){
                    //System.out.println(name+"---"+value);
                    queryWrapper.eq(name,value);
                }
            }
        }
        return queryWrapper;
    }
    private Object getValue(T model,Field f){
        f.setAccessible(true);
        // 获取属性的类型
        String type = f.getGenericType().toString();
        try {
            // 如果type是类类型，则前面包含"class "，后面跟类名
            if ("class java.lang.String".equals(type)) {
                //获取属性值
                String value = (String) f.get(model);
                if("".equals(value)){
                    return null;
                }
                return value;
            }
            if ("class java.lang.Integer".equals(type)) {
                //获取属性值
                return (Integer) f.get(model);
            }
            if ("class java.lang.Long".equals(type)) {
                //获取属性值
                return (Long) f.get(model);
            }
            if ("class java.lang.Double".equals(type)) {
                return (Double) f.get(model);
            }
            if ("class java.lang.Float".equals(type)) {
                return (Float) f.get(model);
            }
            if ("class java.lang.Boolean".equals(type)) {
                return (Boolean) f.get(model);
            }
            if ("class java.util.Date".equals(type)) {
                return (Date) f.get(model);
            }
            // 如果有需要,可以仿照上面继续进行扩充,再增加对其它类型的判断
        } catch (SecurityException | IllegalAccessException | IllegalArgumentException e) {
            e.printStackTrace();
        }
        return null;
        //下面是通过getXXX()方法获取的，不太灵活
//        // 获取属性的名字
//        String name = f.getName();
//        // 将属性的首字符大写，方便构造get，set方法
//        name = name.substring(0, 1).toUpperCase() + name.substring(1);
//        // 获取属性的类型
//        String type = f.getGenericType().toString();
//        try {
//            // 如果type是类类型，则前面包含"class "，后面跟类名
//            if ("class java.lang.String".equals(type)) {
//                Method m = model.getClass().getMethod("get" + name);
//                System.out.println((String) m.invoke(model));
//                return (String) m.invoke(model);
//            }
//            if ("class java.lang.Integer".equals(type)) {
//                Method m = model.getClass().getMethod("get" + name);
//                return (Integer) m.invoke(model);
//            }
//            if ("class java.lang.Long".equals(type)) {
//                Method m = model.getClass().getMethod("get" + name);
//                System.out.println((Long) m.invoke(model));
//                return (Long) m.invoke(model);
//            }
//            if ("class java.lang.Double".equals(type)) {
//                Method m = model.getClass().getMethod("get" + name);
//                return  (Double) m.invoke(model);
//            }
//            if ("class java.lang.Float".equals(type)) {
//                Method m = model.getClass().getMethod("get" + name);
//                return  (Float) m.invoke(model);
//            }
//            if ("class java.lang.Boolean".equals(type)) {
//                Method m = model.getClass().getMethod("get" + name);
//                return  (Boolean) m.invoke(model);
//            }
//            if ("class java.util.Date".equals(type)) {
//                Method m = model.getClass().getMethod("get" + name);
//                return  (Date) m.invoke(model);
//            }
//            // 如果有需要,可以仿照上面继续进行扩充,再增加对其它类型的判断
//        }catch (NoSuchMethodException e) {
//            e.printStackTrace();
//        } catch (SecurityException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (IllegalArgumentException e) {
//            e.printStackTrace();
//        } catch (InvocationTargetException e) {
//            e.printStackTrace();
//        }
//        return null;
    }
}
