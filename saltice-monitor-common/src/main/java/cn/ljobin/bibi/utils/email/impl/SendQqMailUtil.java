package cn.ljobin.bibi.utils.email.impl;

/**
 * @program: guns
 * @description:
 * @author: Mr.Liu
 * @create: 2019-09-12 18:02
 **/

import cn.ljobin.bibi.domain.monitor.MonitorMessage;
import cn.ljobin.bibi.utils.Log4JavaMail;
import cn.ljobin.bibi.utils.email.BaseMailSend;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

@Service("sendQqMailUtil")
@Slf4j
public class SendQqMailUtil implements BaseMailSend {
    private String sendUserEmail = "2018214546@qq.com";
    private String qqPassword = "ujpiarknosuybadb";
    private static volatile Properties properties;
    private static Properties getPropertiesInstance(){
       if(properties==null){
           synchronized (Properties.class){
               if(properties==null){
                   Properties properties2 = new Properties();
                   // 连接协议
                   properties2.put("mail.transport.protocol", "smtp");
                   // 主机名
                   properties2.put("mail.smtp.host", "smtp.qq.com");
                   // 端口号
                   properties2.put("mail.smtp.port", 465);
                   properties2.put("mail.smtp.auth", "true");
                   // 设置是否使用ssl安全连接 ---一般都使用
                   properties2.put("mail.smtp.ssl.enable", "true");
                   // 设置是否显示debug信息 true 会在控制台显示相关信息
                   properties2.put("mail.debug", "false");
                   properties = properties2;
               }
           }
       }
        return properties;
    }
    private ExecutorService executorService = Executors.newFixedThreadPool(15);

    /**
     * TODO 这里应该对相同标题和内容的邮箱进行整合，一起发送
     * @param title 邮件标题
     * @param content 邮件类容
     * @param linkInfo 邮件接收端地址
     */
    @Override
    public void sendEmail(MonitorMessage monitorMessage){
        executorService.submit(()->{
            Transport transport=null;
            Session session=null;
            try {
                // 得到回话对象
                session = Session.getInstance(getPropertiesInstance());
                session.setDebug(false);
                session.setDebugOut(new Log4JavaMail(log,"utf-8"));
                // 获取邮件对象
                Message message = new MimeMessage(session);
                // 设置发件人邮箱地址
                message.setFrom(new InternetAddress(sendUserEmail));
                // 设置收件人邮箱地址
                message.setRecipients(Message.RecipientType.TO, new InternetAddress[]{new InternetAddress(monitorMessage.getLinkInfo())});
                //message.setRecipient(Message.RecipientType.TO, new InternetAddress("xxx@qq.com"));//一个收件人
                // 设置邮件标题
                message.setSubject(monitorMessage.getTitle());
                // 设置邮件内容
                message.setText(monitorMessage.getPloy());
                // 得到邮差对象
                transport = session.getTransport();
                // 连接自己的邮箱账户
                // 密码为QQ邮箱开通的stmp服务后得到的客户端授权码
                //授权码申请参考：https://baijiahao.baidu.com/s?id=1552315463915496&wfr=spider&for=pc
                transport.connect(sendUserEmail, qqPassword);
                // 发送邮件
                transport.sendMessage(message, message.getAllRecipients());
            }catch (MessagingException e){
                log.error(e.toString());
            }finally {
                if(transport!=null){
                    try {
                        transport.close();
                    } catch (MessagingException e) {
                        log.error(e.toString());
                    }
                }
            }
        });
    }
}
