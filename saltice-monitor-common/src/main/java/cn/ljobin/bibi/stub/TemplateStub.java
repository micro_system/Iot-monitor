package cn.ljobin.bibi.stub;

import cn.ljobin.bibi.utils.email.BaseMailSend;
import com.itranswarp.compiler.JavaStringCompiler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

/**
 * @program: Iot-Monitor
 * @description: 模板
 * @author: Mr.Liu
 * @create: 2020-06-06 23:03
 **/
@Service
@Slf4j
public class TemplateStub implements StubFactory{
    private final static String STUB_SOURCE_TEMPLATE =
            "package cn.ljobin.bibi.utils.email.impl;\n" +
            "import cn.ljobin.bibi.utils.email.impl.*;\n" +
            "import cn.ljobin.bibi.domain.monitor.MonitorMessage;\n" +
            "import cn.ljobin.bibi.enums.MessageType;\n" +
            "import java.util.concurrent.Executors;\n" +
            "import java.util.concurrent.LinkedBlockingQueue;\n" +
            "import java.util.concurrent.ScheduledExecutorService;\n" +
            "import java.util.concurrent.TimeUnit;\n" +
            //TODO  Import
            "%s\n"+
            //TODO ClassName
            "public class %s implements BaseMailSend {\n" +
                    "private static String buffer;\n" +
                    "private static String sendUserEmail = \"2018214546@qq.com\";\n" +
                    "    private static String qqPassword = \"ujpiarknosuybadb\";\n" +
                    "/**保存从activemq获取的消息*1024*1024 = 1048576*/\n" +
                    "    private static final LinkedBlockingQueue<MonitorMessage> monitorMessages = new LinkedBlockingQueue<>(2<<20);\n" +
                    "    /**上面那个满了就保存到下面这个**/\n" +
                    "    private static final LinkedBlockingQueue<MonitorMessage> monitorMessages2 = new LinkedBlockingQueue<>(2<<20);\n" +
                    "    private static final ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(10);\n" +
                    //TODO Parem
                    "%s\n" +
                     //TODO Static
                     "static{%s}\n" +
                    "    static {\n" +
                    "        String fileName = \"pod-scale-alarm.html\";\n" +
                    "        InputStream inputStream = ClassLoader.getSystemResourceAsStream(fileName);\n" +
                    "        if(inputStream==null){\n" +
                    "            System.err.println(\"文件读取失败\"+fileName);\n" +
                    "        }else {\n" +
                    "            BufferedReader fileReader = new BufferedReader(new InputStreamReader(inputStream));\n" +
                    "            StringBuilder buffers = new StringBuilder();\n" +
                    "            String line = \"\";\n" +
                    "            try {\n" +
                    "                while ((line = fileReader.readLine()) != null) {\n" +
                    "                    buffers.append(line);\n" +
                    "                }\n" +
                    "                buffer = buffers.toString();\n" +
                    "            } catch (Exception e) {\n" +
                    "                System.err.println(\"读取文件失败，fileName:\"+fileName+ e.toString());\n" +
                    "            } finally {\n" +
                    "                try {\n" +
                    "                    inputStream.close();\n" +
                    "                    fileReader.close();\n" +
                    "                } catch (IOException e) {\n" +
                    "                    e.printStackTrace();\n" +
                    "                }\n" +
                    "            }\n" +
                    "        }" +
                    "        for (int i = 0; i < 10; i++) {\n" +
                    "            scheduledExecutorService.scheduleWithFixedDelay(()->{\n" +
                    "                //弹出队顶元素，队列为空时返回null\n" +
                    "                MonitorMessage monitorMessage = monitorMessages.poll();\n" +
                    "                if(monitorMessage!=null){\n" +
                    "                    send(monitorMessage.getTitle(),\n" +
                    "                            monitorMessage.getPloy(),\n" +
                    "                            monitorMessage.getLinkInfo(),\n" +
                    "                            monitorMessage.getMessageType());\n" +
                    "                }\n" +
                    "            },0,1000, TimeUnit.MILLISECONDS);\n" +
                    "        }\n" +
                    "        scheduledExecutorService.scheduleWithFixedDelay(()->{\n" +
                    "            //弹出队顶元素，队列为空时返回null\n" +
                    "            MonitorMessage monitorMessage = monitorMessages2.poll();\n" +
                    "            if(monitorMessage!=null){\n" +
                    "                send(monitorMessage.getTitle(),\n" +
                    "                        monitorMessage.getPloy(),\n" +
                    "                        monitorMessage.getLinkInfo(),\n" +
                    "                        monitorMessage.getMessageType());\n" +
                    "            }\n" +
                    "        },0,10, TimeUnit.SECONDS);\n" +
                    "    }" +
                    "    @Override\n" +
                    "    public void sendEmail(MonitorMessage monitorMessage) {\n" +
                            "     try {\n" +
                            "         monitorMessages.add(monitorMessage);\n" +
                            "     }catch (IllegalStateException e){\n" +
                            "         monitorMessages2.offer(monitorMessage);\n" +
                            "     }" +
                    "    }\n" +
                            //TODO 下面是要添加的方法 MethodContent
                    "    public static void send(String title, String content, String linkInfo, int type) {\n" +
                            "System.err.println(\"send begin\");\n" +
                    "     %s\n" +
                            "System.err.println(\"send ok\");\n" +
                    "    }\n" +
                    "private static String buildContent(String title,String body,int isDanger) throws IOException {\n" +
                    "            String contentText = body+\"</br>以下是物联网设备详细信息, 敬请查看.<br>below is the information of service instance scale, please check. \";\n" +
                    "            String header = \"<td>分区(Namespace)</td><td>服务(Service)</td><td>伸缩结果(Scale Result)</td><td>伸缩原因(Scale Reason)</td><td>当前实例数(Pod instance number)</td>\";\n" +
                    "            String emailHeadColor;\n" +
                    "            switch (isDanger){\n" +
                    "                case MessageType.ERROR:\n" +
                    "                    emailHeadColor = \"#cc1d0c\";\n" +
                    "                    break;\n" +
                    "                case MessageType.SUCCESS:\n" +
                    "                    emailHeadColor = \"#10fa81\";\n" +
                    "                    break;\n" +
                    "                case MessageType.WANNER:\n" +
                    "                    emailHeadColor = \"yellow\";\n" +
                    "                    break;\n" +
                    "                default:\n" +
                    "                    emailHeadColor = \"#aaa\";\n" +
                    "            }\n" +
                    "            String date = DateFormatUtils.format(new Date(), \"yyyy/MM/dd HH:mm:ss\");\n" +
                    "            String linesBuffer = \"<tr><td>湖南分区</td><td>集群服务</td><td>服务暂停</td><td>异常</td><td>2</td></tr>\";\n" +
                    "            String htmlText = MessageFormat.format(buffer, title,emailHeadColor, contentText, date, header, linesBuffer);\n" +
                    "            htmlText = htmlText.replaceAll(\"<td>\", \"<td style=\\\"padding:6px 10px; line-height: 150%%;\\\">\");\n" +
                    "            htmlText = htmlText.replaceAll(\"<tr>\", \"<tr style=\\\"border-bottom: 1px solid #eee; color:#666;\\\">\");" +
                    "            return htmlText;\n" +
                    "        }" +
            "}";

    @Override
    public <T> T createStub(String imports ,String className,String parem,String statics , String methodContent) throws Exception{
        className = className + "Stub";
        String source = String.format(STUB_SOURCE_TEMPLATE, imports, className, parem,statics, methodContent);
        String stubFullName = "cn.ljobin.bibi.utils.email.impl." + className;
        //编译源代码
        JavaStringCompiler compiler = new JavaStringCompiler();
        try {
            Map<String, byte[]> results = compiler.compile(className + ".java", source);
            // 加载编译好的类
            Class<?> clazz = compiler.loadClass(stubFullName, results);
            // 把实例化
            BaseMailSend stubInstance = (BaseMailSend) clazz.newInstance();
            log.info(stubInstance.toString());
            // 返回这个桩
            return (T) stubInstance;
        } catch (IOException | ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            throw new RuntimeException(e);
        }
    }
}
