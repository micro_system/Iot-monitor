package cn.ljobin.bibi.stub;

/**
 * @program: Iot-Monitor
 * @description: 动态创建邮件代理服务
 * @author: Mr.Liu
 * @create: 2020-06-06 22:52
 **/

public interface StubFactory {
    <T> T createStub(String imports ,String className,String parem,String statics , String methodContent)  throws Exception;
}
